#from django.views.generic.simple import direct_to_template
from django.http import HttpResponseRedirect

def index_page(request):
  template_config = {
  }
  #return direct_to_template(request, 'index.html', extra_content=template_config)
  return HttpResponseRedirect('/listing')
