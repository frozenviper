from django.conf.urls.defaults import *

urlpatterns = patterns('listing',
    (r'^search$', 'views.search'),
    (r'^submit$', 'views.submit'),
    (r'^submit/city$', 'views.submit_city'),
    (r'^submit/location$', 'views.submit_location'),
    (r'^submit/confirm/([\w\d]+)$', 'views.submit_confirm'),
    (r'^remove/([\w\d]+)$', 'views.remove'),
    (r'^remove/confirm$', 'views.remove_confirm'),
    (r'^db/([\w\d]+)/?$', 'views.detail'),
    (r'^([\w-]+)/?$', 'views.index_city'),
    (r'^([\w-]+)/([\w-]+)/?$', 'views.index_location'),
    (r'^/?$', 'views.index'),
)
