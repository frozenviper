from django.views.generic.simple import direct_to_template
from django.views.generic.create_update import create_object
from django.views.generic.create_update import update_object
from django.views.generic.create_update import delete_object
from django.views.generic.list_detail import object_list
from django.views.generic.list_detail import object_detail
from django.http import HttpResponse, HttpResponseRedirect

from listing.models import Listing, City, Location
from listing.forms import ListingForm, CityForm, LocationForm

def __listing(request, queryset, extra_context=dict()):
  listing_info = {
    'queryset': queryset,
    'template_name': 'listing/index.html',
    'extra_context': extra_context,
  }
  return object_list(request, **listing_info)


def index(request):
  queryset = Listing.objects.all()
  return __listing(request, queryset)


def index_city(request, city_slug):
  queryset = Listing.objects.filter(city__slug=city_slug)

  try:
    city = City.objects.get(slug=city_slug)
  except City.DoesNotExist:
    request.flash['error'] = 'Unable to find city with slug: %s' % city_slug
    return HttpResponseRedirect('/listing')

  return __listing(request, queryset, { 'city': city })


def index_location(request, city_slug, location_slug):
  queryset = Listing.objects.filter(city__slug=city_slug).filter(location__slug=location_slug)

  try:
    city = City.objects.get(slug=city_slug)
  except City.DoesNotExist:
    request.flash['error'] = 'Unable to find city with slug: %s' % city_slug
    return HttpResponseRedirect('/listing')

  try:
    location = Location.objects.get(slug=location_slug)
  except Location.DoesNotExist:
    request.flash['error'] = 'Unable to find location with slug: %s' % location_slug
    return HttpResponseRedirect('/listing/%s' % city_slug)

  return __listing(request, queryset, { 'city': city, 'location': location })


def detail(request, listing_id):
  try:
    return object_detail(request, Listing.objects, object_id=listing_id, template_name='listing/detail.html')
  except Listing.DoesNotExist:
    request.flash['error'] = 'Unable to find listing with key: %s' % listing_id
    return HttpResponseRedirect('/listing')


def submit(request):
  return create_object(request, Listing, template_name='listing/submit.html', post_save_redirect='/listing/submit/confirm/%(id)s', form_class=ListingForm) 


def submit_confirm(request, listing_id):
  listing = Listing.objects.get(id=listing_id)
  request.flash['success'] = 'Listing submitted'
  return HttpResponseRedirect('/listing/db/%s' % listing.id)


def submit_city(request):
  return create_object(request, City, template_name='listing/submit_city.html', post_save_redirect='/listing/submit', form_class=CityForm)


def submit_location(request):
  return create_object(request, Location, template_name='listing/submit_location.html', post_save_redirect='/listing/submit', form_class=LocationForm)


def remove(request, listing_id):
  try:
    listing = Listing.objects.get(id=listing_id)
  except Listing.DoesNotExist:
    request.flash['notice'] = 'Could not find listing'
    return HttpResponseRedirect('/listing')
  # todo: need to ensure only owner can delete listing
  return delete_object(request, Listing, post_delete_redirect='/listing/remove/confirm', object_id=listing.id, template_name='listing/remove.html')


def remove_confirm(request):
  request.flash['success'] = 'Listing removed'
  return HttpResponseRedirect('/listing')


def search(request):
  return direct_to_template(request, 'listing/search.html')
