from django.db import models
from django.template.defaultfilters import slugify

class City(models.Model):
  name = models.CharField(max_length=20)
  slug = models.SlugField(max_length=20, editable=False)

  def __unicode__(self):
    return self.name

  def save(self, **kwargs):
    self.slug = slugify(self.name)
    super(City, self).save(**kwargs)

  def absolute_url_for(self):
    return '/listing/%s' % self.slug

class Location(models.Model):
  name = models.CharField(max_length=20)
  slug = models.SlugField(max_length=20, editable=False)
  city = models.ForeignKey(City)

  def __unicode__(self):
    return self.name

  def save(self, **kwargs):
    self.slug = slugify(self.name)
    super(Location, self).save(**kwargs)

  def absolute_url_for(self):
    return '/listing/%s/%s' % (self.city.slug, self.slug)

class Listing(models.Model):
  title = models.CharField("Title", max_length=250)
  description = models.TextField('Description')
  available = models.DateField('Date Available', blank=True, null=True)
  created = models.DateTimeField('DateTime Created', auto_now_add=True, editable=False)
  updated = models.DateTimeField('DateTime Last Updated', auto_now=True, editable=False)
  city = models.ForeignKey(City)
  location = models.ForeignKey(Location)

  def absolute_url_for(self):
    return '/listing/db/%s' % self.id
