from django.forms import ModelForm
from django.forms.widgets import TextInput, Textarea
from django.forms.extras.widgets import SelectDateWidget
from listing.models import City, Location, Listing

class CityForm(ModelForm):
  class Meta:
    model = City
    widgets = {
      'name': TextInput({'class': 'text'}),
    }

class LocationForm(ModelForm):
  class Meta:
    model = Location
    widgets = {
      'name': TextInput({'class': 'text'}),
    }

class ListingForm(ModelForm):
  class Meta:
    model = Listing
    widgets = {
      'title': TextInput({'class': 'text'}),
      'description': Textarea({'class': 'text'}),
      'available': SelectDateWidget(),
    }
