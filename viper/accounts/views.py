from django.contrib.auth import views as auth_views
from django.http import HttpResponse, HttpResponseRedirect

from accounts.forms import AuthenticationForm

def login(request):
  return auth_views.login(request, template_name='accounts/login.html', authentication_form=AuthenticationForm)

def logout(request):
  return auth_views.logout(request, next_page='/')

def index(request):
  if not request.user.is_authenticated():
    pass
  pass
