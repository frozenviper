from django.contrib.auth import forms as auth_forms
from django.forms.fields import CharField
from django.forms.widgets import TextInput, PasswordInput
from django.utils.translation import ugettext_lazy as _

text_input_args = {
  'class': 'text',
}

class AuthenticationForm(auth_forms.AuthenticationForm):
  username = CharField(label=_('Username'), max_length=30, widget=TextInput({'class': 'text'}))
  password = CharField(label=_('Password'), widget=PasswordInput({'class': 'text'}))
