from django.conf.urls.defaults import *

urlpatterns = patterns('accounts',
    (r'^login/', 'views.login'),
    (r'^logout/', 'views.logout'),
    (r'^profile/', 'views.profile'),
    (r'^/?$', 'views.index'),
)
